import sys,tty,termios

from rrb3 import *
from random import randint


#rr = RRB3(8, 6)
rr = RRB3(9, 6) #9v battery, 6v motor





class _Getch:
    def __call__(self):
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(3)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch

def get():
        inkey = _Getch()
        while(1):
                k=inkey()
                if k!='':break
	#print "I just received " + k
	#if k=='\x1b[1~':
        #       print "Home"
	#elif k=='\x1b[5~':
        #       print "pgUp"
	#elif k=='\x1b[4~':
        #       print "End"
	#elif k=='\x1b[6~':
        #       print "PgDn"
        if k=='\x1b[A':
                print "up"
                rr.forward(0.5)
        elif k=='\x1b[B':
                print "down"
                rr.reverse(0.5)
        elif k=='\x1b[C':
                print "right"
                rr.right(0.5)
        elif k=='\x1b[D':
                print "left"
                rr.left(0.5)
        elif k=='\x1bOF':
                print "quit"
        else:
                print "not an arrow key!"

def main():
        for i in range(0,20):
                get()
        #while True:
        #        get()

if __name__=='__main__':
        main()
